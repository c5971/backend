<?php

namespace Database\Factories;

use App\Models\Company;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyFactory extends Factory
{
    protected $model = Company::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "name" => "Comercio ".$this->faker->words(3, true),
            "cnpj" => $this->faker->numberBetween(10000000000000, 99999999999999),
            "address" => "Rua ".$this->faker->words(4, true),
        ];
    }
}
