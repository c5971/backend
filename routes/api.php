<?php
use App\Http\Controllers\UserController;
use App\Http\Controllers\CompanyController;
use Illuminate\Support\Facades\Route;

Route::prefix('user')->group(function () {
    Route::get("/", [UserController::class, 'index'])->name("user.index");
    Route::get("/search/{query}/{field?}", [UserController::class, 'search'])->name("user.search");
    Route::get("/{id}", [UserController::class, 'show'])->name("user.show");
    Route::post("/", [UserController::class, 'create'])->name("user.create");
    Route::put("/{id}", [UserController::class, 'update'])->name("user.update");
    Route::patch("/{id}", [UserController::class, 'replace'])->name("user.replace");
    Route::patch("/{id}/company/{company}", [UserController::class, 'join'])->name("user.join");
    Route::delete("/{id}", [UserController::class, 'delete'])->name("user.delete");
    Route::delete("/{id}/company/{company}", [UserController::class, 'detach'])->name("user.detach");
});

Route::prefix('company')->group(function () {
    Route::get("/", [CompanyController::class, 'index'])->name("company.index");
    Route::get("/search/{query}/{field?}", [CompanyController::class, 'search'])->name("company.search");
    Route::get("/{id}", [CompanyController::class, 'show'])->name("company.show");
    Route::post("/", [CompanyController::class, 'create'])->name("company.create");
    Route::put("/{id}", [CompanyController::class, 'update'])->name("company.update");
    Route::patch("/{id}", [CompanyController::class, 'replace'])->name("company.replace");
    Route::patch("/{id}/user/{user}", [CompanyController::class, 'join'])->name("company.join");
    Route::delete("/{id}", [CompanyController::class, 'delete'])->name("company.delete");
    Route::delete("/{id}/user/{user}", [CompanyController::class, 'detach'])->name("company.detach");
});


