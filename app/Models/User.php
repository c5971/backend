<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Company;

class User extends Model
{
    use HasFactory;

    protected $table = "user";

    protected $fillable = [
        'name',
        'email',
        'telephone',
        'birth',
        'city'
    ];

    public $timestamps = false;

    public function companies()
    {
        return $this->belongsToMany(Company::class, "user_company");
    }
}
