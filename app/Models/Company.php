<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\User;
use App\Models\UserCompany;

class Company extends Model
{
    use HasFactory;

    protected $table = "company";

    protected $fillable = [
        'name',
        'cnpj',
        'address'
    ];

    public $timestamps = false;

    public function users()
    {
        return $this->belongsToMany(User::class, "user_company");
    }
}
