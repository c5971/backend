<?php

namespace App\Http\Controllers;

use App\Http\Requests\CompanyRequest;

use App\Models\Company;
use App\Models\User;

class CompanyController extends Controller
{
    public function index()
    {
        return Company::with('users')->orderBy("id", "desc")->paginate(10);
    }

    public function search($query, $field = "name")
    {
        return Company::with('users')->where($field, 'like', "%{$query}%")->orderBy("id", "desc")->paginate(10);
    }

    public function show($id)
    {
        return Company::with('users')->findOrFail($id);
    }

    public function create(CompanyRequest $req)
    {
        $company = Company::with("users")->create([
            "name" => $req->name,
            "cnpj" => $req->cnpj,
            "address" => $req->address,
        ]);

        $company->refresh();
        
        return $company;
    }

    public function update(CompanyRequest $req)
    {
        $company = Company::with("users")->findOrFail($req->id);
        $company->name = $req->name;
        $company->cnpj = $req->cnpj;
        $company->address = $req->address;

        $company->save();
        $company->refresh();

        return $company;
    }

    public function replace(CompanyRequest $req)
    {
        $company = Company::with("users")->findOrFail($req->id);
        $company->name = $req->name ?? $company->name;
        $company->cnpj = $req->cnpj ?? $company->cnpj;
        $company->address = $req->address ?? $company->address;

        $company->save();
        $company->refresh();

        return $company;
    }

    public function join($id, $user_id)
    {
        $company = Company::with("users")->findOrFail($id);
        $user = User::findOrFail($user_id);

        $company->users()->syncWithoutDetaching([$user->id]);
        $company->refresh();

        return $company;
    }
    public function detach($id, $user_id)
    {
        $company = Company::with("users")->findOrFail($id);
        $user = User::findOrFail($user_id);

        $company->users()->detach([$user->id]);
        $company->refresh();

        return $company;
    }

    public function delete($id)
    {
        Company::findOrFail($id)->delete();
    }
}
