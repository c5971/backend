<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;

use App\Models\Company;
use App\Models\User;

class UserController extends Controller
{
    public function index()
    {
        return User::with("companies")->orderBy("id", "desc")->paginate(10);
    }
    
    public function search($query, $field = "name")
    {
        return User::with('companies')->where($field, 'like', "%{$query}%")->orderBy("id", "desc")->paginate(10);
    }

    public function show($id)
    {
        return User::with("companies")->findOrFail($id);
    }

    public function create(UserRequest $req)
    {
        $user = User::with("companies")->create([
            "name" => $req->name,
            "email" => $req->email,
            "telephone" => $req->telephone,
            "birth" => $req->birth,
            "city" => $req->city,
        ]);

        $user->refresh();
        
        return $user;
    }

    public function update(UserRequest $req)
    {
        $user = User::with("companies")->findOrFail($req->id);
        $user->name = $req->name;
        $user->email = $req->email;
        $user->telephone = $req->telephone;
        $user->birth = $req->birth;
        $user->city = $req->city;

        $user->save();
        $user->refresh();
        
        return $user;
    }

    public function replace(UserRequest $req)
    {
        $user = User::with("companies")->findOrFail($req->id);
        $user->name = $req->name ?? $user->name;
        $user->email = $req->email ?? $user->email;
        $user->telephone = $req->telephone ?? $user->telephone;
        $user->birth = $req->birth ?? $user->birth;
        $user->city = $req->city ?? $user->city;

        $user->save();
        $user->refresh();
        
        return $user;
    }

    public function join($id, $company_id)
    {
        $user = User::with("companies")->findOrFail($id);
        $company = Company::findOrFail($company_id);

        $user->companies()->syncWithoutDetaching([$company->id]);
        $user->refresh();
        
        return $user;
    }
    public function detach($id, $company_id)
    {
        $user = User::with("companies")->findOrFail($id);
        $company = Company::findOrFail($company_id);

        $user->companies()->detach([$company->id]);
        $user->refresh();
        
        return $user;
    }

    public function delete($id)
    {
        User::findOrFail($id)->delete();
    }
}
