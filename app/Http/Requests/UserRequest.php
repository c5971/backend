<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|max:255",
            "email" => "required|max:255|email:rfc",
            "telephone" => "max:255",
            "birth" => "date",
            "city" => "max:255",
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Name is required',
            'name.max' => 'Name is long',
            'email.required' => 'Email is required',
            'email.max' => 'Email is long',
            'email.email' => 'Email is invalid',
            'telephone.max' => 'Telephone is long',
            'birth.date' => 'Birthday is invalid',
            'city.max' => 'City is long',
        ];
    }
}
